from modules.google import GOOGLE
import os
import threading as th

keep_going = True
def key_capture_thread():
    global keep_going
    input()
    keep_going = False

def run_app():
    temp_title = ""
    th.Thread(target=key_capture_thread, args=(), name='key_capture_thread', daemon=True).start()
    try:
        while keep_going:
            g = GOOGLE()
            g.record_wav()
            g.recognize_song()
            g.searchSong()
            try:
                if temp_title != g.song_title and isinstance(g.lyrics, str):
                    
                    os.system('clear')
                    print(g.song_search_url)
                    print(g.song_title)
                    print(g.lyrics)
                    g.save_lyrics()
                    temp_title = g.song_title.strip()
                    # print(temp_title, g.song_title.strip())
            except Exception as err:
                pass
    except KeyboardInterrupt:
        print("exit")

if __name__ == "__main__":
    run_app()
        
