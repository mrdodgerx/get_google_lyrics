
from fake_headers import Headers
from requests import get
import urllib.parse
from bs4 import BeautifulSoup

from os import popen
import sounddevice as sd
from scipy.io.wavfile import write

import time
import os


class GOOGLE():
    def __init__(self) -> None:

        self.fs = 44100
        self.seconds = 10
        self.channels = 3
        self.wav_output = '/tmp/output.wav'

        self.url = 'https://www.google.com/search?q='
        self.header = Headers(os="mac", headers=True).generate()
        self.song_title = None
        self.song_url = None
        self.song_search_url = None
        self.lyrics = None

        self.save_lyrics_dirs = "/home/mrdodgerx/Music/LyricsGoogle"
    
    def record_wav(self):
        wav_record = sd.rec(int(self.seconds * self.fs), samplerate=self.fs, channels=self.channels)
        sd.wait()  # Wait until recording is finished
        write(self.wav_output, self.fs, wav_record)  # Save as WAV file 
    
    def recognize_song(self):
        self.song_title = popen(f'songrec recognize {self.wav_output}').read()
        # print(self.song_title )
    
    def output_html(self,text):
        f = open("google.html", "w")
        f.write(text)
        f.close()
    
    def searchSong(self):
        lyric_div_tag = None
        song_title_url = urllib.parse.quote(f'{self.song_title} lyrics')
        self.song_search_url = f'{self.url }{song_title_url}'
        #print(url)
        r = get(self.song_search_url, headers=self.header)
        if r.status_code == 200:
            # self.output_html(r.text) # debug with htlm code
            soup = BeautifulSoup(r.text, 'html.parser')
            for s in ['Z1hOCe', 'WbKHeb']:
                lyric_div = soup.find_all("div", {"class": s}) # WbKHeb
                if len(lyric_div) == 0:
                    pass
                else:
                    lyric_div_tag = remove_tags(lyric_div[0])
                    break
        else:
            return f'Error in request. Status code: {r.status_code}\n URL: {self.song_search_url}'
        self.lyrics = lyric_div_tag

    def save_lyrics(self):
        create_folder(self.save_lyrics_dirs)
        f = open(f"{self.save_lyrics_dirs}/{self.song_title}.txt", "w")
        f.write(f"{self.lyrics}")
        f.close()




def remove_tags(soup):

    for div in soup.find_all("div", {'class':'lyrics-to'}): 
        div.decompose()
  
    for data in soup(['style', 'script']):
        # Remove tags
        data.decompose()
    lyrics_Text = '\n'.join(soup.stripped_strings)
    return lyrics_Text

def create_folder(path):
    if not os.path.exists(path):
        os.makedirs(path)

# if __name__ == "__main__":
#     while True:
#         x = GOOGLE()
#         x.record_wav()
#         x.recognize_song()
#         if isinstance(x.searchSong(), str):
#             print(x.searchSong())
#         time.sleep(1)
