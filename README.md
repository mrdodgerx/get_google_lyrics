# get_google_lyrics



## Getting started

This application are base on [songrec](https://github.com/marin-m/SongRec). Please install this first

## Installation 

```
git clone https://gitlab.com/mrdodgerx/get_google_lyrics.git
yay -S songrec # For Arch Linux Only. If you are using other distro please go SongRec page to install
pip install virtualenv # install python virtual environment 
python -m venv venv
source venv/bin/activate  # active the environment
pip install -r requirements.txt # install all library need
```

## Configuration 
![alt text](https://gitlab.com/mrdodgerx/get_google_lyrics/-/raw/main/images/configure_code.png)

- You just need to edit self.output and self.save_lyrics_dirs before compile. If you are using same configuration. Just leave it.

## Compile
```
pyinstaller --onefile main.py -c   # compile it as one file and as console
sudo cp dist/main /bin/lyrics      # move main(the compile code) to /bin/ as lyrics 
```

## Use
- just run is as 'lyrics' on your terminal
- Here some output
![alt text](https://gitlab.com/mrdodgerx/get_google_lyrics/-/raw/main/images/results.png)